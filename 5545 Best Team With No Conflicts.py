# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 06:53:01 2020

@author: Le
"""

import numpy

class Solution(object):
    def bestTeamScore(self, scores, ages):
        """
        :type scores: List[int]
        :type ages: List[int]
        :rtype: int
        """
        n = len(scores)
        allPlayers = [[a, s] for a, s in zip(ages, scores)] # ages as keys
        allPlayers.sort(reverse=True) # sorted by ages, keep older players first
        
        ret = 0
        teamScores = numpy.zeros(n)
        
        for i in range(0, n):
            score = allPlayers[i][1] # 0 is age & 1 is score
            teamScores[i] = score
            for j in range(0, i):
                if(allPlayers[j][1] >= score):
                # include player j if increasing team score
                # age increasing due to being sorted
                    teamScores[i] = max(teamScores[i], teamScores[j] + score)
            ret = max(ret, teamScores[i])
        
        return int(ret)
		
# Not working, kept for storage purpose
class Solution2(object):
    def bestTeamScore(self, scores, ages):
        """
        :type scores: List[int]
        :type ages: List[int]
        :rtype: int
        """
        n = len(scores)
        if(n == 1):
            return scores[0]
        
        conflict = self.checkConflict(scores, ages)
        if(conflict == -1):
            return numpy.sum(scores)
        else:
            if(conflict == 0):
                return self.bestTeamScore(scores[1:], ages[1:])
            elif(conflict == (n-1)):
                return self.bestTeamScore(scores[:(n-1)], ages[:(n-1)])
            else:
                firstScores = scores[:conflict-1] + scores[conflict+1:]
                firstAges = ages[:conflict-1] + ages[conflict+1:]
                secondScore = scores[conflict]
                firstScore = self.bestTeamScore(firstScores, firstAges)
                if(firstScore > secondScore):
                    return firstScore
                else:
                    return secondScore
    
    def checkConflict(self, scoreSet, ageSet):
        n = len(scoreSet)
        for i in range(0, n):
            for j in range(0, n):
                if((scoreSet[i] > scoreSet[j]) and (ageSet[i] < ageSet[j])):
                    return i
                
        return -1 # no conflict