# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 05:56:49 2020

@author: Le
"""

import numpy as np

class Solution(object):
    def isFair(self, nums, ignore):
        n = len(nums)
        sumEven = 0
        sumOdd = 0
        sums = np.zeros(n)
        for i in range(ignore):
            if(i % 2 == 0):
                sumEven = sumEven + nums[i]
            else:
                sumOdd = sumOdd + nums[i]
        
        for i in range(ignore + 1, n):
            if((i - 1) % 2 == 0):
                sumEven = sumEven + nums[i]
            else:
                sumOdd = sumOdd + nums[i]
        
        if(sumOdd == sumEven):
            return True
        return False
            
    def waysToMakeFair(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        sumEven = 0
        sumOdd = 0
        sums = np.zeros(n)
        ret = 0
        for i in range(n):
            if(self.isFair(nums, i)):
                ret = ret + 1
        
        return ret
    
sol = Solution()
s = [2,1,6,4]
t = sol.waysToMakeFair(s)
print(t) 