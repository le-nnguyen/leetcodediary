# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 23:00:45 2020

@author: Le
"""

def trap(height):
        
    result = 0
    
    n = len(height)
    
    start = findStart(height)
    end = findEnd(height)
    s = sum(height)  
    while(s > 1):
        for i in range(start, end + 1):
            if(height[i] == 0):
                result = result + 1
            else:
                height[i] = height[i] - 1
                s = s - 1
                #print(s)
        start = findStart(height)
        end = findEnd(height)
        print(str(start) + '-->' + str(end))
                
    return result
    
def findStart(height):
    n = len(height)
    for i in range(0, n):
        if(height[i] > 0):
            return i

def findEnd(height):
    n = len(height) - 1
    for i in range(n, -1, -1):
        if(height[i] > 0):
            return i
        
result = trap([8,2,3,5])
print(result)