# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 00:14:35 2020

@author: Le
"""

class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        n = len(nums)
        freqs = {}
        res  = []   # return result
        resSet = set([]) # use tuple to check duplicate triple
        if(n < 3):
            return []
        for i in range(n):
            if(nums[i] in freqs):
                freqs[nums[i]] = freqs[nums[i]] + 1
            else:
                freqs[nums[i]] = 1
                
        if 0 in freqs:
            if(freqs[0] >= 3):
                res.append([0, 0, 0])
                resSet.add((0, 0, 0))
              
        for i in freqs:
            for j in freqs:
                if(i != j):
                    c = -i-j
                    if(c == i or c == j): # if there are more occurrences
                        if(freqs[c] > 1):
                            triple = [i, j, c]
                            triple.sort()
                            if(not tuple(triple) in resSet):
                                resSet.add(tuple(triple))
                                res.append(triple)
                    elif(c in freqs):
                        triple = [i, j, c]
                        triple.sort()
                        if(not tuple(triple) in resSet):
                            resSet.add(tuple(triple))
                            res.append(triple)
        return res